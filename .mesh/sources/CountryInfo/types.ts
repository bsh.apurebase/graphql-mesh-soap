// @ts-nocheck

import { InContextSdkMethod } from '@graphql-mesh/types';
import { MeshContext } from '@graphql-mesh/runtime';

export namespace CountryInfoTypes {
  export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  /** The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text. */
  String: string;
  /** The `Boolean` scalar type represents `true` or `false`. */
  Boolean: boolean;
  /** The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1. */
  Int: number;
  /** The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point). */
  Float: number;
  /** A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar. */
  DateTime: Date | string;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
};

export type Query = {
  tns_PublicV4_PublicV4Soap_ListHcpApprox4?: Maybe<tns_ListHcpApproxResponse4>;
  tns_PublicV4_PublicV4Soap_GetRibizReferenceData?: Maybe<tns_GetRibizReferenceDataResponse>;
  tns_PublicV4_PublicV4Soap12_ListHcpApprox4?: Maybe<tns_ListHcpApproxResponse4>;
  tns_PublicV4_PublicV4Soap12_GetRibizReferenceData?: Maybe<tns_GetRibizReferenceDataResponse>;
};


export type Querytns_PublicV4_PublicV4Soap_ListHcpApprox4Args = {
  listHcpApproxRequest?: InputMaybe<tns_ListHcpApproxRequest_Input>;
};


export type Querytns_PublicV4_PublicV4Soap_GetRibizReferenceDataArgs = {
  GetRibizReferenceData?: InputMaybe<tns_GetRibizReferenceData_Input>;
};


export type Querytns_PublicV4_PublicV4Soap12_ListHcpApprox4Args = {
  listHcpApproxRequest?: InputMaybe<tns_ListHcpApproxRequest_Input>;
};


export type Querytns_PublicV4_PublicV4Soap12_GetRibizReferenceDataArgs = {
  GetRibizReferenceData?: InputMaybe<tns_GetRibizReferenceData_Input>;
};

export type tns_ListHcpApproxResponse4 = {
  ListHcpApprox: tns_ArrayOfListHcpApprox4;
};

export type tns_ArrayOfListHcpApprox4 = {
  ListHcpApprox4: Array<Maybe<tns_ListHcpApprox4>>;
};

export type tns_ListHcpApprox4 = {
  BirthSurname: Scalars['String'];
  MailingName: Scalars['String'];
  Prefix: Scalars['String'];
  Initial: Scalars['String'];
  Gender: Scalars['String'];
  WorkAddress1: tns_Address;
  WorkAddress2: tns_Address;
  WorkAddress3: tns_Address;
  ArticleRegistration: tns_ArrayOfArticleRegistrationExtApp;
  Specialism: tns_ArrayOfSpecialismExtApp1;
  Mention: tns_ArrayOfMentionExtApp;
  JudgmentProvision: tns_ArrayOfJudgmentProvisionExtApp;
  Limitation: tns_ArrayOfLimitationExtApp;
};

export type tns_Address = {
  AddressTo: Scalars['String'];
  StreetName: Scalars['String'];
  HouseNumber: Scalars['String'];
  HouseNumberAddition: Scalars['String'];
  PostalCode: Scalars['String'];
  City: Scalars['String'];
  ForeignAddress: Scalars['String'];
  CountryCode: Scalars['Float'];
};

export type tns_ArrayOfArticleRegistrationExtApp = {
  ArticleRegistrationExtApp: Array<Maybe<tns_ArticleRegistrationExtApp>>;
};

export type tns_ArticleRegistrationExtApp = {
  ArticleRegistrationNumber: Scalars['Float'];
  ArticleRegistrationStartDate: Scalars['DateTime'];
  ArticleRegistrationEndDate: Scalars['DateTime'];
  ProfessionalGroupCode: Scalars['String'];
};

export type tns_ArrayOfSpecialismExtApp1 = {
  SpecialismExtApp1: Array<Maybe<tns_SpecialismExtApp1>>;
};

export type tns_SpecialismExtApp1 = {
  SpecialismId: Scalars['Float'];
  ArticleRegistrationNumber: Scalars['Float'];
  TypeOfSpecialismId: Scalars['Float'];
};

export type tns_ArrayOfMentionExtApp = {
  MentionExtApp: Array<Maybe<tns_MentionExtApp>>;
};

export type tns_MentionExtApp = {
  MentionId: Scalars['Float'];
  ArticleRegistrationNumber: Scalars['Float'];
  TypeOfMentionId: Scalars['Float'];
  StartDate: Scalars['DateTime'];
  EndDate: Scalars['DateTime'];
};

export type tns_ArrayOfJudgmentProvisionExtApp = {
  JudgmentProvisionExtApp: Array<Maybe<tns_JudgmentProvisionExtApp>>;
};

export type tns_JudgmentProvisionExtApp = {
  ArticleNumber: Scalars['Float'];
  Id: Scalars['Float'];
  StartDate: Scalars['DateTime'];
  PublicDescription: Scalars['String'];
  EndDate: Scalars['DateTime'];
  Public: Scalars['Boolean'];
};

export type tns_ArrayOfLimitationExtApp = {
  LimitationExtApp: Array<Maybe<tns_LimitationExtApp>>;
};

export type tns_LimitationExtApp = {
  LimitationId: Scalars['Float'];
  ArticleRegistrationNumber: Scalars['Float'];
  CompetenceRegistrationId: Scalars['Float'];
  TypeLimitationId: Scalars['Float'];
  Description: Scalars['String'];
  StartDate: Scalars['DateTime'];
  EndDate: Scalars['DateTime'];
  ExpirationEndDate: Scalars['DateTime'];
  MonthsValid: Scalars['Int'];
  YearsValid: Scalars['Int'];
};

export type tns_ListHcpApproxRequest_Input = {
  WebSite?: InputMaybe<Scalars['String']>;
  Name?: InputMaybe<Scalars['String']>;
  Initials?: InputMaybe<Scalars['String']>;
  Prefix?: InputMaybe<Scalars['String']>;
  Street?: InputMaybe<Scalars['String']>;
  Gender?: InputMaybe<Scalars['String']>;
  HouseNumber?: InputMaybe<Scalars['String']>;
  Postalcode?: InputMaybe<Scalars['String']>;
  City?: InputMaybe<Scalars['String']>;
  RegistrationNumber?: InputMaybe<Scalars['String']>;
  DateOfBirth?: InputMaybe<Scalars['String']>;
  ProfessionalGroup?: InputMaybe<Scalars['String']>;
  TypeOfSpecialism?: InputMaybe<Scalars['String']>;
};

export type tns_GetRibizReferenceDataResponse = {
  GetRibizReferenceDataResult: tns_GetRibizReferenceDataResponse;
};

export type tns_GetRibizReferenceData_Input = {
  getCibgReferenceDataRequest?: InputMaybe<Scalars['JSON']>;
};

  export type QuerySdk = {
      /** undefined **/
  tns_PublicV4_PublicV4Soap_ListHcpApprox4: InContextSdkMethod<Query['tns_PublicV4_PublicV4Soap_ListHcpApprox4'], Querytns_PublicV4_PublicV4Soap_ListHcpApprox4Args, MeshContext>,
  /** undefined **/
  tns_PublicV4_PublicV4Soap_GetRibizReferenceData: InContextSdkMethod<Query['tns_PublicV4_PublicV4Soap_GetRibizReferenceData'], Querytns_PublicV4_PublicV4Soap_GetRibizReferenceDataArgs, MeshContext>,
  /** undefined **/
  tns_PublicV4_PublicV4Soap12_ListHcpApprox4: InContextSdkMethod<Query['tns_PublicV4_PublicV4Soap12_ListHcpApprox4'], Querytns_PublicV4_PublicV4Soap12_ListHcpApprox4Args, MeshContext>,
  /** undefined **/
  tns_PublicV4_PublicV4Soap12_GetRibizReferenceData: InContextSdkMethod<Query['tns_PublicV4_PublicV4Soap12_GetRibizReferenceData'], Querytns_PublicV4_PublicV4Soap12_GetRibizReferenceDataArgs, MeshContext>
  };

  export type MutationSdk = {
    
  };

  export type SubscriptionSdk = {
    
  };

  export type Context = {
      ["CountryInfo"]: { Query: QuerySdk, Mutation: MutationSdk, Subscription: SubscriptionSdk },
      
    };
}
