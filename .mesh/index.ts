// @ts-nocheck
import { GraphQLResolveInfo, SelectionSetNode, FieldNode, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { findAndParseConfig } from '@graphql-mesh/cli';
import { createMeshHTTPHandler, MeshHTTPHandler } from '@graphql-mesh/http';
import { getMesh, ExecuteMeshFn, SubscribeMeshFn, MeshContext as BaseMeshContext, MeshInstance } from '@graphql-mesh/runtime';
import { MeshStore, FsStoreStorageAdapter } from '@graphql-mesh/store';
import { path as pathModule } from '@graphql-mesh/cross-helpers';
import { ImportFn } from '@graphql-mesh/types';
import type { CountryInfoTypes } from './sources/CountryInfo/types';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };



/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  /** The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text. */
  String: string;
  /** The `Boolean` scalar type represents `true` or `false`. */
  Boolean: boolean;
  /** The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1. */
  Int: number;
  /** The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point). */
  Float: number;
  /** A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar. */
  DateTime: Date | string;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
};

export type Query = {
  tns_PublicV4_PublicV4Soap_ListHcpApprox4?: Maybe<tns_ListHcpApproxResponse4>;
  tns_PublicV4_PublicV4Soap_GetRibizReferenceData?: Maybe<tns_GetRibizReferenceDataResponse>;
  tns_PublicV4_PublicV4Soap12_ListHcpApprox4?: Maybe<tns_ListHcpApproxResponse4>;
  tns_PublicV4_PublicV4Soap12_GetRibizReferenceData?: Maybe<tns_GetRibizReferenceDataResponse>;
};


export type Querytns_PublicV4_PublicV4Soap_ListHcpApprox4Args = {
  listHcpApproxRequest?: InputMaybe<tns_ListHcpApproxRequest_Input>;
};


export type Querytns_PublicV4_PublicV4Soap_GetRibizReferenceDataArgs = {
  GetRibizReferenceData?: InputMaybe<tns_GetRibizReferenceData_Input>;
};


export type Querytns_PublicV4_PublicV4Soap12_ListHcpApprox4Args = {
  listHcpApproxRequest?: InputMaybe<tns_ListHcpApproxRequest_Input>;
};


export type Querytns_PublicV4_PublicV4Soap12_GetRibizReferenceDataArgs = {
  GetRibizReferenceData?: InputMaybe<tns_GetRibizReferenceData_Input>;
};

export type tns_ListHcpApproxResponse4 = {
  ListHcpApprox: tns_ArrayOfListHcpApprox4;
};

export type tns_ArrayOfListHcpApprox4 = {
  ListHcpApprox4: Array<Maybe<tns_ListHcpApprox4>>;
};

export type tns_ListHcpApprox4 = {
  BirthSurname: Scalars['String'];
  MailingName: Scalars['String'];
  Prefix: Scalars['String'];
  Initial: Scalars['String'];
  Gender: Scalars['String'];
  WorkAddress1: tns_Address;
  WorkAddress2: tns_Address;
  WorkAddress3: tns_Address;
  ArticleRegistration: tns_ArrayOfArticleRegistrationExtApp;
  Specialism: tns_ArrayOfSpecialismExtApp1;
  Mention: tns_ArrayOfMentionExtApp;
  JudgmentProvision: tns_ArrayOfJudgmentProvisionExtApp;
  Limitation: tns_ArrayOfLimitationExtApp;
};

export type tns_Address = {
  AddressTo: Scalars['String'];
  StreetName: Scalars['String'];
  HouseNumber: Scalars['String'];
  HouseNumberAddition: Scalars['String'];
  PostalCode: Scalars['String'];
  City: Scalars['String'];
  ForeignAddress: Scalars['String'];
  CountryCode: Scalars['Float'];
};

export type tns_ArrayOfArticleRegistrationExtApp = {
  ArticleRegistrationExtApp: Array<Maybe<tns_ArticleRegistrationExtApp>>;
};

export type tns_ArticleRegistrationExtApp = {
  ArticleRegistrationNumber: Scalars['Float'];
  ArticleRegistrationStartDate: Scalars['DateTime'];
  ArticleRegistrationEndDate: Scalars['DateTime'];
  ProfessionalGroupCode: Scalars['String'];
};

export type tns_ArrayOfSpecialismExtApp1 = {
  SpecialismExtApp1: Array<Maybe<tns_SpecialismExtApp1>>;
};

export type tns_SpecialismExtApp1 = {
  SpecialismId: Scalars['Float'];
  ArticleRegistrationNumber: Scalars['Float'];
  TypeOfSpecialismId: Scalars['Float'];
};

export type tns_ArrayOfMentionExtApp = {
  MentionExtApp: Array<Maybe<tns_MentionExtApp>>;
};

export type tns_MentionExtApp = {
  MentionId: Scalars['Float'];
  ArticleRegistrationNumber: Scalars['Float'];
  TypeOfMentionId: Scalars['Float'];
  StartDate: Scalars['DateTime'];
  EndDate: Scalars['DateTime'];
};

export type tns_ArrayOfJudgmentProvisionExtApp = {
  JudgmentProvisionExtApp: Array<Maybe<tns_JudgmentProvisionExtApp>>;
};

export type tns_JudgmentProvisionExtApp = {
  ArticleNumber: Scalars['Float'];
  Id: Scalars['Float'];
  StartDate: Scalars['DateTime'];
  PublicDescription: Scalars['String'];
  EndDate: Scalars['DateTime'];
  Public: Scalars['Boolean'];
};

export type tns_ArrayOfLimitationExtApp = {
  LimitationExtApp: Array<Maybe<tns_LimitationExtApp>>;
};

export type tns_LimitationExtApp = {
  LimitationId: Scalars['Float'];
  ArticleRegistrationNumber: Scalars['Float'];
  CompetenceRegistrationId: Scalars['Float'];
  TypeLimitationId: Scalars['Float'];
  Description: Scalars['String'];
  StartDate: Scalars['DateTime'];
  EndDate: Scalars['DateTime'];
  ExpirationEndDate: Scalars['DateTime'];
  MonthsValid: Scalars['Int'];
  YearsValid: Scalars['Int'];
};

export type tns_ListHcpApproxRequest_Input = {
  WebSite?: InputMaybe<Scalars['String']>;
  Name?: InputMaybe<Scalars['String']>;
  Initials?: InputMaybe<Scalars['String']>;
  Prefix?: InputMaybe<Scalars['String']>;
  Street?: InputMaybe<Scalars['String']>;
  Gender?: InputMaybe<Scalars['String']>;
  HouseNumber?: InputMaybe<Scalars['String']>;
  Postalcode?: InputMaybe<Scalars['String']>;
  City?: InputMaybe<Scalars['String']>;
  RegistrationNumber?: InputMaybe<Scalars['String']>;
  DateOfBirth?: InputMaybe<Scalars['String']>;
  ProfessionalGroup?: InputMaybe<Scalars['String']>;
  TypeOfSpecialism?: InputMaybe<Scalars['String']>;
};

export type tns_GetRibizReferenceDataResponse = {
  GetRibizReferenceDataResult: tns_GetRibizReferenceDataResponse;
};

export type tns_GetRibizReferenceData_Input = {
  getCibgReferenceDataRequest?: InputMaybe<Scalars['JSON']>;
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string | ((fieldNode: FieldNode) => SelectionSetNode);
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;



/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Query: ResolverTypeWrapper<{}>;
  tns_ListHcpApproxResponse4: ResolverTypeWrapper<tns_ListHcpApproxResponse4>;
  tns_ArrayOfListHcpApprox4: ResolverTypeWrapper<tns_ArrayOfListHcpApprox4>;
  tns_ListHcpApprox4: ResolverTypeWrapper<tns_ListHcpApprox4>;
  String: ResolverTypeWrapper<Scalars['String']>;
  tns_Address: ResolverTypeWrapper<tns_Address>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  tns_ArrayOfArticleRegistrationExtApp: ResolverTypeWrapper<tns_ArrayOfArticleRegistrationExtApp>;
  tns_ArticleRegistrationExtApp: ResolverTypeWrapper<tns_ArticleRegistrationExtApp>;
  DateTime: ResolverTypeWrapper<Scalars['DateTime']>;
  tns_ArrayOfSpecialismExtApp1: ResolverTypeWrapper<tns_ArrayOfSpecialismExtApp1>;
  tns_SpecialismExtApp1: ResolverTypeWrapper<tns_SpecialismExtApp1>;
  tns_ArrayOfMentionExtApp: ResolverTypeWrapper<tns_ArrayOfMentionExtApp>;
  tns_MentionExtApp: ResolverTypeWrapper<tns_MentionExtApp>;
  tns_ArrayOfJudgmentProvisionExtApp: ResolverTypeWrapper<tns_ArrayOfJudgmentProvisionExtApp>;
  tns_JudgmentProvisionExtApp: ResolverTypeWrapper<tns_JudgmentProvisionExtApp>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  tns_ArrayOfLimitationExtApp: ResolverTypeWrapper<tns_ArrayOfLimitationExtApp>;
  tns_LimitationExtApp: ResolverTypeWrapper<tns_LimitationExtApp>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  tns_ListHcpApproxRequest_Input: tns_ListHcpApproxRequest_Input;
  tns_GetRibizReferenceDataResponse: ResolverTypeWrapper<tns_GetRibizReferenceDataResponse>;
  tns_GetRibizReferenceData_Input: tns_GetRibizReferenceData_Input;
  JSON: ResolverTypeWrapper<Scalars['JSON']>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Query: {};
  tns_ListHcpApproxResponse4: tns_ListHcpApproxResponse4;
  tns_ArrayOfListHcpApprox4: tns_ArrayOfListHcpApprox4;
  tns_ListHcpApprox4: tns_ListHcpApprox4;
  String: Scalars['String'];
  tns_Address: tns_Address;
  Float: Scalars['Float'];
  tns_ArrayOfArticleRegistrationExtApp: tns_ArrayOfArticleRegistrationExtApp;
  tns_ArticleRegistrationExtApp: tns_ArticleRegistrationExtApp;
  DateTime: Scalars['DateTime'];
  tns_ArrayOfSpecialismExtApp1: tns_ArrayOfSpecialismExtApp1;
  tns_SpecialismExtApp1: tns_SpecialismExtApp1;
  tns_ArrayOfMentionExtApp: tns_ArrayOfMentionExtApp;
  tns_MentionExtApp: tns_MentionExtApp;
  tns_ArrayOfJudgmentProvisionExtApp: tns_ArrayOfJudgmentProvisionExtApp;
  tns_JudgmentProvisionExtApp: tns_JudgmentProvisionExtApp;
  Boolean: Scalars['Boolean'];
  tns_ArrayOfLimitationExtApp: tns_ArrayOfLimitationExtApp;
  tns_LimitationExtApp: tns_LimitationExtApp;
  Int: Scalars['Int'];
  tns_ListHcpApproxRequest_Input: tns_ListHcpApproxRequest_Input;
  tns_GetRibizReferenceDataResponse: tns_GetRibizReferenceDataResponse;
  tns_GetRibizReferenceData_Input: tns_GetRibizReferenceData_Input;
  JSON: Scalars['JSON'];
}>;

export type soapDirectiveArgs = {
  elementName?: Maybe<Scalars['String']>;
  bindingNamespace?: Maybe<Scalars['String']>;
  endpoint?: Maybe<Scalars['String']>;
};

export type soapDirectiveResolver<Result, Parent, ContextType = MeshContext, Args = soapDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type QueryResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  tns_PublicV4_PublicV4Soap_ListHcpApprox4?: Resolver<Maybe<ResolversTypes['tns_ListHcpApproxResponse4']>, ParentType, ContextType, Partial<Querytns_PublicV4_PublicV4Soap_ListHcpApprox4Args>>;
  tns_PublicV4_PublicV4Soap_GetRibizReferenceData?: Resolver<Maybe<ResolversTypes['tns_GetRibizReferenceDataResponse']>, ParentType, ContextType, Partial<Querytns_PublicV4_PublicV4Soap_GetRibizReferenceDataArgs>>;
  tns_PublicV4_PublicV4Soap12_ListHcpApprox4?: Resolver<Maybe<ResolversTypes['tns_ListHcpApproxResponse4']>, ParentType, ContextType, Partial<Querytns_PublicV4_PublicV4Soap12_ListHcpApprox4Args>>;
  tns_PublicV4_PublicV4Soap12_GetRibizReferenceData?: Resolver<Maybe<ResolversTypes['tns_GetRibizReferenceDataResponse']>, ParentType, ContextType, Partial<Querytns_PublicV4_PublicV4Soap12_GetRibizReferenceDataArgs>>;
}>;

export type tns_ListHcpApproxResponse4Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ListHcpApproxResponse4'] = ResolversParentTypes['tns_ListHcpApproxResponse4']> = ResolversObject<{
  ListHcpApprox?: Resolver<ResolversTypes['tns_ArrayOfListHcpApprox4'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ArrayOfListHcpApprox4Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArrayOfListHcpApprox4'] = ResolversParentTypes['tns_ArrayOfListHcpApprox4']> = ResolversObject<{
  ListHcpApprox4?: Resolver<Array<Maybe<ResolversTypes['tns_ListHcpApprox4']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ListHcpApprox4Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ListHcpApprox4'] = ResolversParentTypes['tns_ListHcpApprox4']> = ResolversObject<{
  BirthSurname?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  MailingName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  Prefix?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  Initial?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  Gender?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  WorkAddress1?: Resolver<ResolversTypes['tns_Address'], ParentType, ContextType>;
  WorkAddress2?: Resolver<ResolversTypes['tns_Address'], ParentType, ContextType>;
  WorkAddress3?: Resolver<ResolversTypes['tns_Address'], ParentType, ContextType>;
  ArticleRegistration?: Resolver<ResolversTypes['tns_ArrayOfArticleRegistrationExtApp'], ParentType, ContextType>;
  Specialism?: Resolver<ResolversTypes['tns_ArrayOfSpecialismExtApp1'], ParentType, ContextType>;
  Mention?: Resolver<ResolversTypes['tns_ArrayOfMentionExtApp'], ParentType, ContextType>;
  JudgmentProvision?: Resolver<ResolversTypes['tns_ArrayOfJudgmentProvisionExtApp'], ParentType, ContextType>;
  Limitation?: Resolver<ResolversTypes['tns_ArrayOfLimitationExtApp'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_AddressResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_Address'] = ResolversParentTypes['tns_Address']> = ResolversObject<{
  AddressTo?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  StreetName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  HouseNumber?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  HouseNumberAddition?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  PostalCode?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  City?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  ForeignAddress?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  CountryCode?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ArrayOfArticleRegistrationExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArrayOfArticleRegistrationExtApp'] = ResolversParentTypes['tns_ArrayOfArticleRegistrationExtApp']> = ResolversObject<{
  ArticleRegistrationExtApp?: Resolver<Array<Maybe<ResolversTypes['tns_ArticleRegistrationExtApp']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ArticleRegistrationExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArticleRegistrationExtApp'] = ResolversParentTypes['tns_ArticleRegistrationExtApp']> = ResolversObject<{
  ArticleRegistrationNumber?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  ArticleRegistrationStartDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  ArticleRegistrationEndDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  ProfessionalGroupCode?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface DateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['DateTime'], any> {
  name: 'DateTime';
}

export type tns_ArrayOfSpecialismExtApp1Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArrayOfSpecialismExtApp1'] = ResolversParentTypes['tns_ArrayOfSpecialismExtApp1']> = ResolversObject<{
  SpecialismExtApp1?: Resolver<Array<Maybe<ResolversTypes['tns_SpecialismExtApp1']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_SpecialismExtApp1Resolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_SpecialismExtApp1'] = ResolversParentTypes['tns_SpecialismExtApp1']> = ResolversObject<{
  SpecialismId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  ArticleRegistrationNumber?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  TypeOfSpecialismId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ArrayOfMentionExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArrayOfMentionExtApp'] = ResolversParentTypes['tns_ArrayOfMentionExtApp']> = ResolversObject<{
  MentionExtApp?: Resolver<Array<Maybe<ResolversTypes['tns_MentionExtApp']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_MentionExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_MentionExtApp'] = ResolversParentTypes['tns_MentionExtApp']> = ResolversObject<{
  MentionId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  ArticleRegistrationNumber?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  TypeOfMentionId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  StartDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  EndDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ArrayOfJudgmentProvisionExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArrayOfJudgmentProvisionExtApp'] = ResolversParentTypes['tns_ArrayOfJudgmentProvisionExtApp']> = ResolversObject<{
  JudgmentProvisionExtApp?: Resolver<Array<Maybe<ResolversTypes['tns_JudgmentProvisionExtApp']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_JudgmentProvisionExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_JudgmentProvisionExtApp'] = ResolversParentTypes['tns_JudgmentProvisionExtApp']> = ResolversObject<{
  ArticleNumber?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  Id?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  StartDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  PublicDescription?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  EndDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  Public?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_ArrayOfLimitationExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_ArrayOfLimitationExtApp'] = ResolversParentTypes['tns_ArrayOfLimitationExtApp']> = ResolversObject<{
  LimitationExtApp?: Resolver<Array<Maybe<ResolversTypes['tns_LimitationExtApp']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_LimitationExtAppResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_LimitationExtApp'] = ResolversParentTypes['tns_LimitationExtApp']> = ResolversObject<{
  LimitationId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  ArticleRegistrationNumber?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  CompetenceRegistrationId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  TypeLimitationId?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  Description?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  StartDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  EndDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  ExpirationEndDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  MonthsValid?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  YearsValid?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type tns_GetRibizReferenceDataResponseResolvers<ContextType = MeshContext, ParentType extends ResolversParentTypes['tns_GetRibizReferenceDataResponse'] = ResolversParentTypes['tns_GetRibizReferenceDataResponse']> = ResolversObject<{
  GetRibizReferenceDataResult?: Resolver<ResolversTypes['tns_GetRibizReferenceDataResponse'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface JSONScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export type Resolvers<ContextType = MeshContext> = ResolversObject<{
  Query?: QueryResolvers<ContextType>;
  tns_ListHcpApproxResponse4?: tns_ListHcpApproxResponse4Resolvers<ContextType>;
  tns_ArrayOfListHcpApprox4?: tns_ArrayOfListHcpApprox4Resolvers<ContextType>;
  tns_ListHcpApprox4?: tns_ListHcpApprox4Resolvers<ContextType>;
  tns_Address?: tns_AddressResolvers<ContextType>;
  tns_ArrayOfArticleRegistrationExtApp?: tns_ArrayOfArticleRegistrationExtAppResolvers<ContextType>;
  tns_ArticleRegistrationExtApp?: tns_ArticleRegistrationExtAppResolvers<ContextType>;
  DateTime?: GraphQLScalarType;
  tns_ArrayOfSpecialismExtApp1?: tns_ArrayOfSpecialismExtApp1Resolvers<ContextType>;
  tns_SpecialismExtApp1?: tns_SpecialismExtApp1Resolvers<ContextType>;
  tns_ArrayOfMentionExtApp?: tns_ArrayOfMentionExtAppResolvers<ContextType>;
  tns_MentionExtApp?: tns_MentionExtAppResolvers<ContextType>;
  tns_ArrayOfJudgmentProvisionExtApp?: tns_ArrayOfJudgmentProvisionExtAppResolvers<ContextType>;
  tns_JudgmentProvisionExtApp?: tns_JudgmentProvisionExtAppResolvers<ContextType>;
  tns_ArrayOfLimitationExtApp?: tns_ArrayOfLimitationExtAppResolvers<ContextType>;
  tns_LimitationExtApp?: tns_LimitationExtAppResolvers<ContextType>;
  tns_GetRibizReferenceDataResponse?: tns_GetRibizReferenceDataResponseResolvers<ContextType>;
  JSON?: GraphQLScalarType;
}>;

export type DirectiveResolvers<ContextType = MeshContext> = ResolversObject<{
  soap?: soapDirectiveResolver<any, any, ContextType>;
}>;

export type MeshContext = CountryInfoTypes.Context & BaseMeshContext;


const baseDir = pathModule.join(typeof __dirname === 'string' ? __dirname : '/', '..');

const importFn: ImportFn = <T>(moduleId: string) => {
  const relativeModuleId = (pathModule.isAbsolute(moduleId) ? pathModule.relative(baseDir, moduleId) : moduleId).split('\\').join('/').replace(baseDir + '/', '');
  switch(relativeModuleId) {
    default:
      return Promise.reject(new Error(`Cannot find module '${relativeModuleId}'.`));
  }
};

const rootStore = new MeshStore('.mesh', new FsStoreStorageAdapter({
  cwd: baseDir,
  importFn,
  fileType: "ts",
}), {
  readonly: true,
  validate: false
});

export function getMeshOptions() {
  console.warn('WARNING: These artifacts are built for development mode. Please run "mesh build" to build production artifacts');
  return findAndParseConfig({
    dir: baseDir,
    artifactsDir: ".mesh",
    configName: "mesh",
    additionalPackagePrefixes: [],
    initialLoggerPrefix: "🕸️  Mesh",
  });
}

export function createBuiltMeshHTTPHandler<TServerContext = {}>(): MeshHTTPHandler<TServerContext> {
  return createMeshHTTPHandler<TServerContext>({
    baseDir,
    getBuiltMesh: getBuiltMesh,
    rawServeConfig: undefined,
  })
}

let meshInstance$: Promise<MeshInstance> | undefined;

export function getBuiltMesh(): Promise<MeshInstance> {
  if (meshInstance$ == null) {
    meshInstance$ = getMeshOptions().then(meshOptions => getMesh(meshOptions)).then(mesh => {
      const id = mesh.pubsub.subscribe('destroy', () => {
        meshInstance$ = undefined;
        mesh.pubsub.unsubscribe(id);
      });
      return mesh;
    });
  }
  return meshInstance$;
}

export const execute: ExecuteMeshFn = (...args) => getBuiltMesh().then(({ execute }) => execute(...args));

export const subscribe: SubscribeMeshFn = (...args) => getBuiltMesh().then(({ subscribe }) => subscribe(...args));